//
//  TaskTaskPresenterTests.swift
//  WeeklyManager
//
//  Created by Pavel Marinchenko on 02/09/2019.
//  Copyright © 2019 pavbox_archive. All rights reserved.
//

import XCTest

class TaskPresenterTest: XCTestCase {

    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }

    class MockInteractor: TaskInteractorInput {

    }

    class MockRouter: TaskRouterInput {

    }

    class MockViewController: TaskViewInput {

        func setupInitialState() {

        }
    }
}
