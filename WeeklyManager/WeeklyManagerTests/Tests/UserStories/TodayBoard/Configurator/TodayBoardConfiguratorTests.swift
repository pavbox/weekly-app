//
//  TodayBoardTodayBoardConfiguratorTests.swift
//  WeeklyManager
//
//  Created by Pavel Marinchenko on 30/08/2019.
//  Copyright © 2019 pavbox_archive. All rights reserved.
//

import XCTest

class TodayBoardModuleConfiguratorTests: XCTestCase {

    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }

    func testConfigureModuleForViewController() {

        //given
        let viewController = TodayBoardViewControllerMock()
        let configurator = TodayBoardModuleConfiguratorTests()

        //when
        configurator.configureModuleForViewInput(viewInput: viewController)

        //then
        XCTAssertNotNil(viewController.output, "TodayBoardViewController is nil after configuration")
        XCTAssertTrue(viewController.output is TodayBoardPresenter, "output is not TodayBoardPresenter")

        let presenter: TodayBoardPresenter = viewController.output as? TodayBoardPresenter
        XCTAssertNotNil(presenter?.view, "view in TodayBoardPresenter is nil after configuration")
        XCTAssertNotNil(presenter?.router, "router in TodayBoardPresenter is nil after configuration")
        XCTAssertTrue(presenter?.router is TodayBoardRouter, "router is not TodayBoardRouter")

        let interactor: TodayBoardInteractor = presenter.interactor as? TodayBoardInteractor
        XCTAssertNotNil(interactor?.output, "output in TodayBoardInteractor is nil after configuration")
    }

    class TodayBoardViewControllerMock: TodayBoardViewController {

        var setupInitialStateDidCall = false

        override func setupInitialState() {
            setupInitialStateDidCall = true
        }
    }
}
