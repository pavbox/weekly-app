//
//  UINibableView.swift
//  WeeklyManager
//
//  Created by Павел Маринченко on 9/10/19.
//  Copyright © 2019 pavbox. All rights reserved.
//

import UIKit

// TODO: Setup as protocol next
class UINibableView: UIView {

    // MARK: Properties

    var view: UIView! {
        return subviews.first
    }

    // MARK: Initialization and deinitialization

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        xibSetup()
    }

    override init(frame: CGRect) {
        super.init(frame: frame)
        xibSetup()
    }

    // MARK: Private methods

    private func xibSetup() {
        if let view = setupView() {
            addSubview(view)
            view.frame = self.bounds
        }
    }

    private func setupView() -> UIView? {
        let viewType = type(of: self)
        let bundle = Bundle(for: viewType)
        let nibName = String(describing: viewType)
        let nib = UINib(nibName: nibName, bundle: bundle)

        guard let view = nib.instantiate(withOwner: self, options: nil).first as? UIView else {
            return nil
        }

        return view
    }
}
