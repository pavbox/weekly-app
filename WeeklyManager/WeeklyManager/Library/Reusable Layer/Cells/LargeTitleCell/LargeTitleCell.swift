//
//  LargeTitleCell.swift
//  WeeklyManager
//
//  Created by Павел Маринченко on 9/1/19.
//  Copyright © 2019 pavbox. All rights reserved.
//

import UIKit

class LargeTitleCell: UITableViewCell {

    // MARK: IBOutlet

    @IBOutlet private weak var titleLabel: UILabel!

    // MARK: Life cycle

    override func awakeFromNib() {
        super.awakeFromNib()
        setupInitialState()
    }

    // MARK: Internal methods

    func configure(text: String) {
        titleLabel.text = text
    }

}

// MARK: - Private methods

private extension LargeTitleCell {

    func setupInitialState() {
        configureView()
        configureLabelView()
    }

    func configureView() {
        selectionStyle = .none
    }

    // TODO: reusable fonts, styles, design
    func configureLabelView() {
        titleLabel.font = .systemFont(ofSize: 36, weight: .heavy)
    }

}
