//
//  LargeTitleCellObject.swift
//  WeeklyManager
//
//  Created by Павел Маринченко on 9/4/19.
//  Copyright © 2019 pavbox. All rights reserved.
//

import Foundation

final class LargeTitleCellObject: NSObject {

    // MARK: Properties

    let title: String

    // MARK: Initialization

    init(title: String) {
        self.title = title
    }

}
