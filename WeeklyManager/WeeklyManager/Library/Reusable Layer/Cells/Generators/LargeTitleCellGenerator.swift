//
//  LargeTitleCellGenerator.swift
//  WeeklyManager
//
//  Created by Павел Маринченко on 9/4/19.
//  Copyright © 2019 pavbox. All rights reserved.
//

import UIKit

final class LargeTitleCellGenerator {

    // MARK: Properties

    private let model: LargeTitleCellObject

    // MARK: Initialization and deinitialization

    init(with model: LargeTitleCellObject) {
        self.model = model
    }

}

// MARK: - CellGenerator

extension LargeTitleCellGenerator: CellGenerator {
    var identifier: UITableViewCell.Type {
        return LargeTitleCell.self
    }
}

// MARK: - ViewBuilder

extension LargeTitleCellGenerator: ViewBuilder {
    func build(view: LargeTitleCell) {
        view.configure(text: model.title)
    }
}
