//
//  CheckboxControlRenderer.swift
//  WeeklyManager
//
//  Created by Павел Маринченко on 9/5/19.
//  Copyright © 2019 pavbox. All rights reserved.
//

import CoreGraphics
import QuartzCore
import UIKit.UIBezierPath

class CheckboxControlRenderer {

    // MARK: Private properties

    private var bounds: CGRect = .zero
    private var inactiveColor: CGColor?
    private var activeColor: CGColor?
    private weak var ctx: CGContext?

    // MARK: Internal methods

    func updateValue(_ value: CheckboxControl.ControlValue) {
        switch value {
        case .checked:
            setupCheckedValue()
        case .half:
            setupHalfCheckedValue()
        case .unchecked:
            setupUncheckedValue()
        }
    }

}

// MARK: - Configuration methods

extension CheckboxControlRenderer {

    func updateBounds(_ bounds: CGRect) {
        self.bounds = bounds
    }

    func updateContext(_ ctx: CGContext?) {
        self.ctx = ctx
    }

    func set(inactiveColor: CGColor?) {
        self.inactiveColor = inactiveColor
    }

    func set(activeColor: CGColor?) {
        self.activeColor = activeColor
    }

}

// MARK: - Private methods

private extension CheckboxControlRenderer {

    func setupCheckedValue() {
        ctx?.addRect(bounds)

        if let inactiveColor = self.inactiveColor {
            ctx?.setFillColor(inactiveColor)
        }

        ctx?.setLineWidth(10)
        ctx?.strokePath()
    }

    func setupHalfCheckedValue() {
        ctx?.addRect(bounds)

        if let inactiveColor = self.inactiveColor {
            ctx?.setFillColor(inactiveColor)
        }

        ctx?.setLineWidth(1)
        ctx?.strokePath()
    }

    func setupUncheckedValue() {
        drawRect(bounds: bounds)

        if let inactiveColor = self.inactiveColor {
            ctx?.setFillColor(inactiveColor)
        }

        ctx?.strokePath()
    }

    func drawRect(bounds: CGRect) {
        let rad: CGFloat = 4
        let halfRad = rad / 2
        let insets = UIEdgeInsets(top: halfRad, left: halfRad, bottom: halfRad, right: halfRad)
        let edgedRect = bounds.inset(by: insets)
        let path = CGPath(roundedRect: edgedRect, cornerWidth: rad, cornerHeight: rad, transform: nil)

        ctx?.setLineWidth(4)
        ctx?.addPath(path)
    }

}
