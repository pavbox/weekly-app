//
//  CheckboxControl.swift
//  WeeklyManager
//
//  Created by Павел Маринченко on 9/5/19.
//  Copyright © 2019 pavbox. All rights reserved.
//

import UIKit

@IBDesignable
class CheckboxControl: UIControl {

    // MARK: - Enums

    enum ControlValue {
        case unchecked
        case checked
        case half
    }

    // MARK: - Properties

    var borderColor: UIColor = .gray
    var activeColor: UIColor = .green

    // MARK: - Private properties

    private var renderer = CheckboxControlRenderer()
    private var value: ControlValue = .unchecked

    // MARK: - Initialization and deinitialization

    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        commonInit()
    }

    func commonInit() {
        backgroundColor = .clear
        addTarget(self, action: #selector(setupState), for: .touchUpInside)
    }

    // MARK: - Life cycle

    override func draw(_ rect: CGRect) {
        let ctx = UIGraphicsGetCurrentContext()
        renderer.updateBounds(rect)
        renderer.updateContext(ctx)
        renderer.set(inactiveColor: borderColor.cgColor)
        renderer.set(activeColor: activeColor.cgColor)
        renderer.updateValue(value)
    }

    // MARK: - Internal methods

    func change(value: CheckboxControl.ControlValue) {
        self.value = value
        setNeedsDisplay()
    }

}

// MARK: - Private methods

private extension CheckboxControl {

    // TODO: Test, call delegate output

    @objc
    func setupState() {
        if value == .unchecked {
            change(value: .half)
        } else if value == .half {
            change(value: .checked)
        } else if value == .checked {
            change(value: .unchecked)
        }
    }

}
