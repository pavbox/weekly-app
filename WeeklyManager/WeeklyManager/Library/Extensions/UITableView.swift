//
//  UITableView.swift
//  WeeklyManager
//
//  Created by Павел Маринченко on 9/4/19.
//  Copyright © 2019 pavbox. All rights reserved.
//

import UIKit

extension UITableView {

    /// Register cell from nib by swift-type
    func register(type: UITableViewCell.Type) {
        let nib = UINib(nibName: type.nameOfClass, bundle: nil)
        self.register(nib, forCellReuseIdentifier: type.nameOfClass)
    }

}
