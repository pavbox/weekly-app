//
//  UITabBarController.swift
//  WeeklyManager
//
//  Created by Павел Маринченко on 8/31/19.
//  Copyright © 2019 pavbox. All rights reserved.
//

import UIKit

extension UITabBarController {

    func setupFloatingState() {

        // TODO: configure items
        // TODO: configure in another place
        let height: CGFloat = 64
        let bottomOffset: CGFloat = self.view.safeAreaInsets.bottom + 10
        let sideOffset: CGFloat = 10
        let radius: CGFloat = 25

        self.tabBar.invalidateIntrinsicContentSize()

        self.tabBar.frame.origin.y = self.view.bounds.height - height - bottomOffset
        self.tabBar.frame.origin.x = sideOffset

        self.tabBar.frame.size.width = self.view.bounds.size.width - sideOffset * 2
        self.tabBar.frame.size.height = height

        self.tabBar.layer.cornerRadius = radius
        self.tabBar.clipsToBounds = true

        self.tabBar.barTintColor = .white
        self.tabBar.tintColor = .gray
        self.tabBar.layer.borderWidth = 1
        self.tabBar.layer.borderColor = UIColor.gray.withAlphaComponent(0.3).cgColor

    }

}
