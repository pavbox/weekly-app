//
//  Foundation.swift
//  WeeklyManager
//
//  Created by Павел Маринченко on 9/4/19.
//  Copyright © 2019 pavbox. All rights reserved.
//

import Foundation

extension NSObject {

    class var nameOfClass: String {
        return String(describing: self)
    }

}
