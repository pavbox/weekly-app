//
//  CollectionProtocols.swift
//  WeeklyManager
//
//  Created by Павел Маринченко on 9/4/19.
//  Copyright © 2019 pavbox. All rights reserved.
//

import UIKit

// NOTE: trying avoid indexes

protocol CellGenerator {
    var identifier: UITableViewCell.Type { get }

    func generate(tableView: UITableView, indexPath: IndexPath) -> UITableViewCell
}

protocol ViewBuilder {
    associatedtype ViewType
    func build(view: ViewType)
}

extension CellGenerator where Self: ViewBuilder {

    func generate(tableView: UITableView, indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: self.identifier.nameOfClass, for: indexPath) as? ViewType else {
            return UITableViewCell()
        }

        build(view: cell)

        return cell as? UITableViewCell ?? UITableViewCell()
    }

}
