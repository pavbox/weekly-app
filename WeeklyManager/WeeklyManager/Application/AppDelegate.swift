//
//  AppDelegate.swift
//  WeeklyManager
//
//  Created by Павел Маринченко on 8/28/19.
//  Copyright © 2019 pavbox. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // TODO: conf by hands
//        let mainStoryBoard = UIStoryboard(name: "Main", bundle: nil)
//        let mainTabBarVC = mainStoryBoard.instantiateInitialViewController()
//        window?.rootViewController = mainTabBarVC
        return true
    }

}
