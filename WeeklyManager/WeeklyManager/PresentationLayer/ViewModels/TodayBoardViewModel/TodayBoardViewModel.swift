//
//  TodayBoardViewModel.swift
//  WeeklyManager
//
//  Created by Павел Маринченко on 9/4/19.
//  Copyright © 2019 pavbox. All rights reserved.
//

import Foundation

struct TodayBoardViewModel {

    // MARK: Properties

    var title: String
    var generators: [CellGenerator]

    // MARK: Initialization and deinitialization

    init() {
        title = ""
        generators = []
    }

    init(title: String, generators: [CellGenerator]) {
        self.title = title
        self.generators = generators
    }
}
