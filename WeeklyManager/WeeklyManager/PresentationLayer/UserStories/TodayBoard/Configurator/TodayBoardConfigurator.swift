//
//  TodayBoardTodayBoardConfigurator.swift
//  WeeklyManager
//
//  Created by Pavel Marinchenko on 30/08/2019.
//  Copyright © 2019 pavbox_archive. All rights reserved.
//

import UIKit

class TodayBoardModuleConfigurator {

    func configureModuleForViewInput<UIViewController>(viewInput: UIViewController) {

        if let viewController = viewInput as? TodayBoardViewController {
            configure(viewController: viewController)
        }
    }

    private func configure(viewController: TodayBoardViewController) {

        let router = TodayBoardRouter()

        let presenter = TodayBoardPresenter()
        presenter.view = viewController
        presenter.router = router

        let interactor = TodayBoardInteractor()
        interactor.output = presenter

        presenter.interactor = interactor
        viewController.output = presenter
    }

}
