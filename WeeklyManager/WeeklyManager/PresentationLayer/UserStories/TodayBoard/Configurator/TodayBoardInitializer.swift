//
//  TodayBoardTodayBoardInitializer.swift
//  WeeklyManager
//
//  Created by Pavel Marinchenko on 30/08/2019.
//  Copyright © 2019 pavbox_archive. All rights reserved.
//

import UIKit

class TodayBoardModuleInitializer: NSObject {

    @IBOutlet weak var todayboardViewController: TodayBoardViewController!

    override func awakeFromNib() {

        let configurator = TodayBoardModuleConfigurator()
        configurator.configureModuleForViewInput(viewInput: todayboardViewController)
    }

}
