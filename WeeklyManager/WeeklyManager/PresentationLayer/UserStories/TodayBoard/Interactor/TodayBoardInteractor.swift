//
//  TodayBoardTodayBoardInteractor.swift
//  WeeklyManager
//
//  Created by Pavel Marinchenko on 30/08/2019.
//  Copyright © 2019 pavbox_archive. All rights reserved.
//

class TodayBoardInteractor: TodayBoardInteractorInput {

    weak var output: TodayBoardInteractorOutput!

    private lazy var viewModel = TodayBoardViewModel()

    func load(_ completion: (TodayBoardViewModel) -> Void) {
        // load from local db
        viewModel.title = "Wedn, Aug 21-27"
        // TODO: setup blocks or presentor's methods
        viewModel.generators.append(TaskTableCellGenerator(with: TaskTableCellObject(title: "PERVIY TASK", value: .unchecked)))
        completion(viewModel)
    }

}
