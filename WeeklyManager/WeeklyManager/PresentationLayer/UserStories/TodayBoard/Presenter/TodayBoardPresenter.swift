//
//  TodayBoardTodayBoardPresenter.swift
//  WeeklyManager
//
//  Created by Pavel Marinchenko on 30/08/2019.
//  Copyright © 2019 pavbox_archive. All rights reserved.
//

class TodayBoardPresenter: TodayBoardModuleInput, TodayBoardViewOutput, TodayBoardInteractorOutput {

    weak var view: TodayBoardViewInput!
    var interactor: TodayBoardInteractorInput!
    var router: TodayBoardRouterInput!

    func viewIsReady() {
        view.setupInitialState()

        interactor.load { [weak self] (model) in
            self?.view.update(with: model)
        }
    }

}

private extension TodayBoardPresenter { }
