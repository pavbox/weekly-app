//
//  TodayBoardCellBuilder.swift
//  WeeklyManager
//
//  Created by Павел Маринченко on 9/2/19.
//  Copyright © 2019 pavbox. All rights reserved.
//

import UIKit

final class TodayBoardCellBuilder {

    // build collections for today screen

    var cells: [CellGenerator] = []

    func buildHeaderCell(with text: String) -> TodayBoardCellBuilder {
        let cellObject = LargeTitleCellObject(title: text)
        cells.append(LargeTitleCellGenerator(with: cellObject))
        return self
    }

}
