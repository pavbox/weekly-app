//
//  TodayBoardTodayBoardViewController.swift
//  WeeklyManager
//
//  Created by Pavel Marinchenko on 30/08/2019.
//  Copyright © 2019 pavbox_archive. All rights reserved.
//

import UIKit

class TodayBoardViewController: UIViewController, TodayBoardViewInput {

    // MARK: - IBOutlet

    @IBOutlet private weak var tableView: UITableView!

    // MARK: - Properties

    var output: TodayBoardViewOutput!
    var ddm: TodayBoardDisplayDataManager?

    // MARK: - Life cycle

    override func viewDidLoad() {
        super.viewDidLoad()
        output.viewIsReady()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(true, animated: true)
    }

    // MARK: - TodayBoardViewInput

    func setupInitialState() {
        configureTitle()
        configureDDM()
    }

    func update(with model: TodayBoardViewModel) {
        // TODO: create base ddm knowing only about cell generators
        var cells = buildCells(from: model)
        cells.append(contentsOf: model.generators)
        ddm?.reload(with: cells)
    }

}

// MARK: - Private Methods

private extension TodayBoardViewController {

    func configureTitle() {
        // TODO: move to interactor
        title = L10n.TodayBoard.title
    }

    func configureDDM() {
        ddm = TodayBoardDisplayDataManager(collection: self.tableView)
    }

    func buildCells(from model: TodayBoardViewModel) -> [CellGenerator] {
        let builder = TodayBoardCellBuilder()
            .buildHeaderCell(with: model.title)
        return builder.cells
    }

}
