//
//  TodayBoardDisplayDataManager.swift
//  WeeklyManager
//
//  Created by Павел Маринченко on 9/2/19.
//  Copyright © 2019 pavbox. All rights reserved.
//

import UIKit

final class TodayBoardDisplayDataManager: NSObject {

    // MARK: - Private properties

    private var tableView: UITableView
    private var generators: [CellGenerator] = []

    // MARK: - Init

    init(collection: UITableView) {
        tableView = collection
        super.init()
        // TODO: create base ddm
        tableView.register(type: LargeTitleCell.self)
        tableView.register(type: TaskTableCell.self)
        tableView.dataSource = self
        tableView.delegate = self
        setupInitialState()
    }

    func reload() {
        tableView.reloadData()
    }

    func reload(with generators: [CellGenerator]) {
        self.generators.removeAll()
        self.generators = generators
        self.tableView.reloadData()
    }

}

// MARK: - UITableViewDataSource

extension TodayBoardDisplayDataManager: UITableViewDataSource {

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return generators.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        return generators[indexPath.row].generate(tableView: tableView, indexPath: indexPath)
    }

}

// MARK: - UITableViewDelegate

extension TodayBoardDisplayDataManager: UITableViewDelegate {

}

// MARK: - Private methods

private extension TodayBoardDisplayDataManager {

    func setupInitialState() {
        tableView.separatorStyle = .none
        tableView.tableHeaderView = UIView()
        tableView.tableFooterView = UIView()
        tableView.backgroundColor = UIColor.clear
    }

}
