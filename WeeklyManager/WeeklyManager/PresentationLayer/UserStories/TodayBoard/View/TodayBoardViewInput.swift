//
//  TodayBoardTodayBoardViewInput.swift
//  WeeklyManager
//
//  Created by Pavel Marinchenko on 30/08/2019.
//  Copyright © 2019 pavbox_archive. All rights reserved.
//

protocol TodayBoardViewInput: class {

    /// setup initial state of the view

    func setupInitialState()

    func update(with generators: TodayBoardViewModel)

}
