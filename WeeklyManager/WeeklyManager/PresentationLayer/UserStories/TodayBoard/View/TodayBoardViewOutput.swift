//
//  TodayBoardTodayBoardViewOutput.swift
//  WeeklyManager
//
//  Created by Pavel Marinchenko on 30/08/2019.
//  Copyright © 2019 pavbox_archive. All rights reserved.
//

protocol TodayBoardViewOutput {
    /// Notify presenter that view is ready
    func viewIsReady()
}
