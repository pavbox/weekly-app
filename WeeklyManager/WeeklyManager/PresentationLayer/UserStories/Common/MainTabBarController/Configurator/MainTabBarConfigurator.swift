//
//  MainTabBarConfigurator.swift
//  WeeklyManager
//
//  Created by Павел Маринченко on 9/1/19.
//  Copyright © 2019 pavbox. All rights reserved.
//

import UIKit

// remove? dont need to init dependencies
class MainTabBarConfigurator {

    func configureModuleForViewInput<UIViewController>(viewInput: UIViewController) {

        if let viewController = viewInput as? MainTabBarController {
            configure(viewController: viewController)
        }
    }

    private func configure(viewController: MainTabBarController) {

        // TODO: setup tabs
//        if let todayBoardVC = TodayBoardModuleInitializer().todayboardViewController {
//            let todayBoardNavC = UINavigationController(rootViewController: todayBoardVC)
//            let tabButton = UITabBarItem(title: "today1", image: nil, selectedImage: nil)
//            todayBoardNavC.tabBarItem = tabButton
//            viewController.viewControllers?.append(todayBoardNavC)
//        }

    }

}
