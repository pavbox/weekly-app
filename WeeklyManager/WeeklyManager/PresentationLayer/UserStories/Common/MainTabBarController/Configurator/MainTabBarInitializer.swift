//
//  MainTabBarInitializer.swift
//  WeeklyManager
//
//  Created by Павел Маринченко on 9/1/19.
//  Copyright © 2019 pavbox. All rights reserved.
//

import UIKit

class MainTabBarInitializer: NSObject {

    @IBOutlet weak var mainTabBarViewController: MainTabBarController!

    override func awakeFromNib() {
        let configurator = MainTabBarConfigurator()
        configurator.configureModuleForViewInput(viewInput: mainTabBarViewController)
    }

}
