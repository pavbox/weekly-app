//
//  MainTabBarController.swift
//  WeeklyManager
//
//  Created by Павел Маринченко on 8/30/19.
//  Copyright © 2019 pavbox. All rights reserved.
//

import UIKit

final class MainTabBarController: UITabBarController {

    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        self.setupFloatingState()
    }

}
