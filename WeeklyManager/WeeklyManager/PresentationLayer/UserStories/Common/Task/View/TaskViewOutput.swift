//
//  TaskTaskViewOutput.swift
//  WeeklyManager
//
//  Created by Pavel Marinchenko on 02/09/2019.
//  Copyright © 2019 pavbox_archive. All rights reserved.
//

protocol TaskViewOutput {
    /// Notify presenter that view is ready
    func viewIsReady()
}
