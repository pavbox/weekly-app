//
//  TaskTaskViewInput.swift
//  WeeklyManager
//
//  Created by Pavel Marinchenko on 02/09/2019.
//  Copyright © 2019 pavbox_archive. All rights reserved.
//

protocol TaskViewInput: class {
    /// setup initial state of the view
    func setupInitialState()
}
