//
//  TaskTaskViewController.swift
//  WeeklyManager
//
//  Created by Pavel Marinchenko on 02/09/2019.
//  Copyright © 2019 pavbox_archive. All rights reserved.
//

import UIKit

class TaskView: UINibableView, TaskViewInput {

    // MARK: IBOutlets

    @IBOutlet private weak var checkboxView: CheckboxControl!
    @IBOutlet private weak var titleLabel: UILabel!

    // MARK: Properties

    var output: TaskViewOutput!

    // MARK: Life cycle

    override func awakeFromNib() {
        super.awakeFromNib()
        output.viewIsReady()
    }

    // MARK: TaskViewInput

    func setupInitialState() {

    }

    func configure(title: String) {
        titleLabel.text = title
    }

    func configure(value: CheckboxControl.ControlValue) {
        checkboxView.change(value: value)
    }

}
