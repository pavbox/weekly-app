//
//  TaskTableCellGenerator.swift
//  WeeklyManager
//
//  Created by Павел Маринченко on 9/9/19.
//  Copyright © 2019 pavbox. All rights reserved.
//

import UIKit

class TaskTableCellGenerator {

    // MARK: Properties

    let model: TaskTableCellObject

    // MARK: Initialization and deinitialization

    init(with model: TaskTableCellObject) {
        self.model = model
    }

}

// MARK: - CellGenerator

extension TaskTableCellGenerator: CellGenerator {
    var identifier: UITableViewCell.Type {
        return TaskTableCell.self
    }
}

// MARK: - ViewBuilder

extension TaskTableCellGenerator: ViewBuilder {
    func build(view: TaskTableCell) {
        view.configure(with: model)
    }
}
