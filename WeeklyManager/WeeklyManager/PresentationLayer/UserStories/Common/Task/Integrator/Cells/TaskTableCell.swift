//
//  TaskTableCell.swift
//  WeeklyManager
//
//  Created by Павел Маринченко on 9/9/19.
//  Copyright © 2019 pavbox. All rights reserved.
//

import UIKit

class TaskTableCell: UITableViewCell {

    // MARK: IBOutlets

    @IBOutlet private weak var taskView: TaskView!

    // MARK: Life cycle

    override func awakeFromNib() {
        super.awakeFromNib()
        setupInitialState()
    }

    // MARK: Internal methods

    func configure(with model: TaskTableCellObject) {
        taskView.configure(title: model.title)
        taskView.configure(value: model.value)
    }
}

// MARK: - Private methods

private extension TaskTableCell {

    func setupInitialState() {
        // TODO: move to generator
        self.selectionStyle = .none
    }

}
