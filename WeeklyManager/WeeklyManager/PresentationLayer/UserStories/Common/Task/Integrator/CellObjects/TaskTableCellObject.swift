//
//  TaskTableCellObject.swift
//  WeeklyManager
//
//  Created by Павел Маринченко on 9/9/19.
//  Copyright © 2019 pavbox. All rights reserved.
//

import Foundation

final class TaskTableCellObject {

    // MARK: Properties

    let title: String
    let value: CheckboxControl.ControlValue

    // MARK: Initialization and deinitialization

    init(title: String, value: CheckboxControl.ControlValue) {
        self.title = title
        self.value = value
    }

}
