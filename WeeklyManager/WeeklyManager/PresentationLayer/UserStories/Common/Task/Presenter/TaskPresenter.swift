//
//  TaskTaskPresenter.swift
//  WeeklyManager
//
//  Created by Pavel Marinchenko on 02/09/2019.
//  Copyright © 2019 pavbox_archive. All rights reserved.
//

class TaskPresenter: TaskModuleInput, TaskViewOutput, TaskInteractorOutput {

    weak var view: TaskViewInput!
    var interactor: TaskInteractorInput!
    var router: TaskRouterInput!

    func viewIsReady() {

    }
}
