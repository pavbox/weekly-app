//
//  TaskTaskConfigurator.swift
//  WeeklyManager
//
//  Created by Pavel Marinchenko on 02/09/2019.
//  Copyright © 2019 pavbox_archive. All rights reserved.
//

import UIKit

class TaskModuleConfigurator {

    func configureModuleForViewInput<UIView>(viewInput: UIView) {

        if let view = viewInput as? TaskView {
            configure(view: view)
        }
    }

    private func configure(view: TaskView) {

        let router = TaskRouter()

        let presenter = TaskPresenter()
        presenter.view = view
        presenter.router = router

        let interactor = TaskInteractor()
        interactor.output = presenter

        presenter.interactor = interactor
        view.output = presenter
    }

}
