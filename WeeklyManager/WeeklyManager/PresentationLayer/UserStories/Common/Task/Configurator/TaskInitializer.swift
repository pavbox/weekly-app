//
//  TaskTaskInitializer.swift
//  WeeklyManager
//
//  Created by Pavel Marinchenko on 02/09/2019.
//  Copyright © 2019 pavbox_archive. All rights reserved.
//

import UIKit

class TaskModuleInitializer: NSObject {

    // Connect with object on storyboard or file owner
    @IBOutlet weak var taskView: TaskView!

    override func awakeFromNib() {

        let configurator = TaskModuleConfigurator()
        configurator.configureModuleForViewInput(viewInput: taskView)
    }

}
